From: Andrew Dornbush <andrew.dornbush@gmail.com>
Date: Thu, 27 Jul 2017 20:48:43 -0400
Subject: add max iterations parameter to trac solver

---
 trac_ik_examples/launch/pr2_arm.launch             |  4 +-
 trac_ik_examples/src/ik_tests.cpp                  | 78 +++++++++-------------
 .../src/trac_ik_kinematics_plugin.cpp              |  2 +-
 trac_ik_lib/include/trac_ik/trac_ik.hpp            | 44 +++---------
 trac_ik_lib/src/trac_ik.cpp                        | 41 ++++++++----
 5 files changed, 74 insertions(+), 95 deletions(-)

diff --git a/trac_ik_examples/launch/pr2_arm.launch b/trac_ik_examples/launch/pr2_arm.launch
index a2c5dfa..e693c7b 100644
--- a/trac_ik_examples/launch/pr2_arm.launch
+++ b/trac_ik_examples/launch/pr2_arm.launch
@@ -3,7 +3,7 @@
   <arg name="num_samples" default="1000" />
   <arg name="chain_start" default="torso_lift_link" />
   <arg name="chain_end" default="r_wrist_roll_link" />
-  <arg name="timeout" default="0.005" />
+  <arg name="max_iterations" default="100" />
 
   <param name="robot_description" command="$(find xacro)/xacro.py '$(find pr2_description)/robots/pr2.urdf.xacro'" />
 
@@ -12,7 +12,7 @@
     <param name="num_samples" value="$(arg num_samples)"/>
     <param name="chain_start" value="$(arg chain_start)"/>
     <param name="chain_end" value="$(arg chain_end)"/>
-    <param name="timeout" value="$(arg timeout)"/>
+    <param name="max_iterations" value="$(arg max_iterations)"/>
     <param name="urdf_param" value="/robot_description"/>
   </node>
 
diff --git a/trac_ik_examples/src/ik_tests.cpp b/trac_ik_examples/src/ik_tests.cpp
index 6bdc6c0..dc524ba 100644
--- a/trac_ik_examples/src/ik_tests.cpp
+++ b/trac_ik_examples/src/ik_tests.cpp
@@ -28,6 +28,7 @@ OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISE
 OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
 
+#include <chrono>
 #include <boost/date_time.hpp>
 #include <kdl/chainiksolverpos_nr_jl.hpp>
 #include <ros/ros.h>
@@ -39,7 +40,7 @@ void test(
     int num_samples,
     const std::string& chain_start,
     const std::string& chain_end,
-    double timeout,
+    int max_iterations,
     const std::string& urdf_param)
 {
     double eps = 1e-5;
@@ -76,23 +77,11 @@ void test(
     ROS_INFO("joint_min: %zu", joint_min.data.size());
     ROS_INFO("joint_max: %zu", joint_max.data.size());
 
-    TRAC_IK::TRAC_IK tracik_solver(chain, joint_min, joint_max, timeout, eps);
+    TRAC_IK::TRAC_IK tracik_solver(
+            chain, joint_min, joint_max, max_iterations, eps, TRAC_IK::Speed);
 
-    bool valid = tracik_solver.getKDLChain(chain);
-
-    if (!valid) {
-        ROS_ERROR("There was no valid KDL chain found");
-        return;
-    }
-
-    KDL::JntArray ll;
-    KDL::JntArray ul;
-    valid = tracik_solver.getKDLLimits(ll, ul);
-
-    if (!valid) {
-        ROS_ERROR("There were no valid KDL joint limits found");
-        return;
-    }
+    auto& ll = tracik_solver.getLowerLimits();
+    auto& ul = tracik_solver.getUpperLimits();
 
     assert(chain.getNrOfJoints() == ll.data.size());
     assert(chain.getNrOfJoints() == ul.data.size());
@@ -100,7 +89,7 @@ void test(
     // Set up KDL IK
     KDL::ChainFkSolverPos_recursive fk_solver(chain); // Forward kin. solver
     KDL::ChainIkSolverVel_pinv vik_solver(chain); // PseudoInverse vel solver
-    KDL::ChainIkSolverPos_NR_JL kdl_solver(chain, ll, ul, fk_solver, vik_solver, 1, eps); // Joint Limit Solver
+    KDL::ChainIkSolverPos_NR_JL kdl_solver(chain, ll, ul, fk_solver, vik_solver, max_iterations, eps); // Joint Limit Solver
     // 1 iteration per solve (will wrap in timed loop to compare with TRAC-IK)
 
     ////////////////////////////////////////////////////////////////////////
@@ -120,19 +109,14 @@ void test(
     std::vector<KDL::JntArray> position_samples;
     KDL::JntArray q(chain.getNrOfJoints());
 
-    for (uint i = 0; i < num_samples; ++i) {
+    for (int i = 0; i < num_samples; ++i) {
         for (uint j = 0; j < ll.data.size(); ++j) {
             q(j) = TRAC_IK::fRand(ll(j), ul(j));
         }
         position_samples.push_back(q);
     }
 
-    boost::posix_time::ptime start_time;
-    boost::posix_time::time_duration diff;
-
     KDL::JntArray result;
-    KDL::Frame end_effector_pose;
-    int rc;
 
     double total_time = 0.0;
     int success = 0;
@@ -142,21 +126,23 @@ void test(
     int tens = -1;
 
     for (int i = 0; i < num_samples; i++) {
+        KDL::Frame end_effector_pose;
         fk_solver.JntToCart(position_samples[i], end_effector_pose);
-        double elapsed = 0;
-        result = nominal; // start with nominal
-        start_time = boost::posix_time::microsec_clock::local_time();
-        do {
-            q = result; // when iterating start with last solution
-            rc = kdl_solver.CartToJnt(q, end_effector_pose,result);
-            diff = boost::posix_time::microsec_clock::local_time() - start_time;
-            elapsed = diff.total_nanoseconds() / 1e9;
-        } while (rc < 0 && elapsed < timeout);
-        total_time += elapsed;
+
+        auto before = std::chrono::high_resolution_clock::now();
+        int rc = kdl_solver.CartToJnt(nominal, end_effector_pose, result);
+        auto after = std::chrono::high_resolution_clock::now();
+
+        // accumulate time taken
+        auto diff = std::chrono::duration<double>(after - before);
+        total_time += diff.count();
+
+        // count success
         if (rc >= 0) {
             ++success;
         }
 
+        // log progress
         int new_tens = i * 10 / num_samples;
         if (new_tens != tens) {
             ROS_INFO_STREAM(i * 100 / num_samples << "\% done");
@@ -164,23 +150,25 @@ void test(
         }
     }
 
-    tens = -1;
-
     ROS_INFO_STREAM("KDL found " << success << " solutions (" << 100.0 * success / num_samples << "\%) with an average of " << total_time / num_samples << " secs per sample");
 
+    tens = -1;
     total_time = 0.0;
     success = 0;
 
     ROS_INFO_STREAM("*** Testing TRAC-IK with " << num_samples << " random samples");
 
     for (int i = 0; i < num_samples; i++) {
+        KDL::Frame end_effector_pose;
         fk_solver.JntToCart(position_samples[i], end_effector_pose);
-        double elapsed = 0;
-        start_time = boost::posix_time::microsec_clock::local_time();
-        rc = tracik_solver.CartToJnt(nominal, end_effector_pose, result);
-        diff = boost::posix_time::microsec_clock::local_time() - start_time;
-        elapsed = diff.total_nanoseconds() / 1e9;
-        total_time += elapsed;
+
+        auto before = std::chrono::high_resolution_clock::now();
+        int rc = tracik_solver.CartToJnt(nominal, end_effector_pose, result);
+        auto after = std::chrono::high_resolution_clock::now();
+
+        auto diff = std::chrono::duration<double>(after - before);
+        total_time += diff.count();
+
         if (rc >= 0) {
             success++;
         }
@@ -205,7 +193,7 @@ int main(int argc, char** argv)
     std::string chain_start;
     std::string chain_end;
     std::string urdf_param;
-    double timeout;
+    int max_iterations;
 
     nh.param("num_samples", num_samples, 1000);
     nh.param("chain_start", chain_start, std::string(""));
@@ -216,14 +204,14 @@ int main(int argc, char** argv)
         exit (-1);
     }
 
-    nh.param("timeout", timeout, 0.005);
+    nh.param("max_iterations", max_iterations, 100);
     nh.param("urdf_param", urdf_param, std::string("/robot_description"));
 
     if (num_samples < 1) {
         num_samples = 1;
     }
 
-    test(nh, num_samples, chain_start, chain_end, timeout, urdf_param);
+    test(nh, num_samples, chain_start, chain_end, max_iterations, urdf_param);
 
     // Useful when you make a script that loops over multiple launch files that test different robot chains
     // std::vector<char *> commandVector;
diff --git a/trac_ik_kinematics_plugin/src/trac_ik_kinematics_plugin.cpp b/trac_ik_kinematics_plugin/src/trac_ik_kinematics_plugin.cpp
index 602e5b0..0448b6b 100644
--- a/trac_ik_kinematics_plugin/src/trac_ik_kinematics_plugin.cpp
+++ b/trac_ik_kinematics_plugin/src/trac_ik_kinematics_plugin.cpp
@@ -297,7 +297,7 @@ bool TRAC_IKKinematicsPlugin::searchPositionIK(
         in(z) = ik_seed_state[z];
     }
 
-    solver_->setTimeout(timeout);
+//    solver_->setTimeout(timeout);
 
     int rc = solver_->CartToJnt(in, frame, out, bounds_);
 
diff --git a/trac_ik_lib/include/trac_ik/trac_ik.hpp b/trac_ik_lib/include/trac_ik/trac_ik.hpp
index 8fffda0..899a6a2 100644
--- a/trac_ik_lib/include/trac_ik/trac_ik.hpp
+++ b/trac_ik_lib/include/trac_ik/trac_ik.hpp
@@ -55,38 +55,17 @@ public:
         const KDL::Chain& _chain,
         const KDL::JntArray& _q_min,
         const KDL::JntArray& _q_max,
-        double _maxtime = 0.005,
+        int max_iters = 100,
         double _eps = 1e-5,
         SolveType _type = Speed);
 
-    // TODO: propagate timeout to underlying solvers
-    void setTimeout(double max_time) { max_time_ = max_time; }
     void setBounds(const KDL::Twist& bounds) { bounds_ = bounds; }
+    const KDL::Twist& getBounds() const { return bounds_; }
 
-    bool getKDLChain(KDL::Chain& chain)
-    {
-        chain = chain_;
-        return true;
-    }
-
-    bool getKDLLimits(KDL::JntArray& lb_, KDL::JntArray& ub_)
-    {
-        lb_ = joint_min_;
-        ub_ = joint_max_;
-        return true;
-    }
-
-    static double JointErr(
-        const KDL::JntArray& arr1,
-        const KDL::JntArray& arr2)
-    {
-        double err = 0;
-        for (uint i = 0; i < arr1.data.size(); i++) {
-            err += pow(arr1(i) - arr2(i), 2);
-        }
-
-        return err;
-    }
+    const KDL::Chain& getKDLChain(KDL::Chain& chain) const { return chain_; }
+
+    const KDL::JntArray& getLowerLimits() const { return joint_min_; }
+    const KDL::JntArray& getUpperLimits() const { return joint_max_; }
 
     /// Return a negative value if an error was encountered.
     int CartToJnt(
@@ -104,18 +83,15 @@ private:
     KDL::JntArray joint_max_;
     std::vector<KDL::BasicJointType> joint_types_;
 
-    double eps_;
-    double max_time_;
-    SolveType solve_type_;
-
-    KDL::Twist bounds_;
-
     KDL::ChainJntToJacSolver jac_solver_;
 
     NLOPT_IK::NLOPT_IK nl_solver_;
     KDL::ChainIkSolverPos_TL ik_solver_;
 
-    boost::posix_time::ptime start_time_;
+    KDL::Twist bounds_;
+    double eps_;
+    SolveType solve_type_;
+    int max_iters_;
 
     std::vector<KDL::JntArray> solutions_;
     std::vector<std::pair<double, size_t>> errors_;
diff --git a/trac_ik_lib/src/trac_ik.cpp b/trac_ik_lib/src/trac_ik.cpp
index 9430780..115ede9 100644
--- a/trac_ik_lib/src/trac_ik.cpp
+++ b/trac_ik_lib/src/trac_ik.cpp
@@ -47,24 +47,39 @@ OF THE POSSIBILITY OF SUCH DAMAGE.
 
 namespace TRAC_IK {
 
+inline double JointErr(
+    const KDL::JntArray& q1,
+    const KDL::JntArray& q2)
+{
+    double err = 0;
+    for (uint i = 0; i < q1.data.size(); i++) {
+        err += pow(q1(i) - q2(i), 2);
+    }
+
+    return err;
+}
+
 TRAC_IK::TRAC_IK(
     const KDL::Chain& chain,
     const KDL::JntArray& q_min,
     const KDL::JntArray& q_max,
-    double max_time,
+    int max_iterations,
     double eps,
     SolveType type)
 :
     chain_(chain),
     joint_min_(q_min),
     joint_max_(q_max),
+    joint_types_(),
+    jac_solver_(chain),
+    nl_solver_(chain, q_min, q_max, eps, NLOPT_IK::SumSq),
+    ik_solver_(chain, q_min, q_max, eps, true, true),
+    bounds_(KDL::Twist::Zero()),
     eps_(eps),
-    max_time_(max_time),
     solve_type_(type),
-    bounds_(KDL::Twist::Zero()),
-    jac_solver_(chain),
-    nl_solver_(chain, joint_min_, joint_max_, eps_, NLOPT_IK::SumSq),
-    ik_solver_(chain, joint_min_, joint_max_, eps_, true, true)
+    max_iters_(max_iterations),
+    solutions_(),
+    errors_()
 {
     assert(chain_.getNrOfJoints() == joint_min_.data.size());
     assert(chain_.getNrOfJoints() == joint_max_.data.size());
@@ -286,7 +301,7 @@ int TRAC_IK::CartToJnt(
 
     int solver = SOLVER_NLOPT;
 
-    const int max_iters = 250;
+    const int max_iters = max_iters_;
     for (int i = 0; i < max_iters; ++i) {
         // interleave iterations of kdl and nl opt
         switch (solver) {
@@ -318,14 +333,14 @@ int TRAC_IK::CartToJnt(
                     solutions_.push_back(q_out);
                     double err;
                     switch (solve_type_) {
-                    case Manip1: {
+                    case Manip1:
                         err = manipPenalty(q_out) * TRAC_IK::ManipValue1(q_out);
-                    }   break;
-                    case Manip2: {
+                        break;
+                    case Manip2:
                         err = manipPenalty(q_out) * TRAC_IK::ManipValue2(q_out);
-                       }   break;
+                        break;
                     default:
-                        err = TRAC_IK::JointErr(q_init, q_out);
+                        err = JointErr(q_init, q_out);
                         break;
                     }
 
@@ -379,7 +394,7 @@ int TRAC_IK::CartToJnt(
                         err = manipPenalty(q_out) * TRAC_IK::ManipValue2(q_out);
                         break;
                     default:
-                        err = TRAC_IK::JointErr(q_init, q_out);
+                        err = JointErr(q_init, q_out);
                         break;
                     }
 
