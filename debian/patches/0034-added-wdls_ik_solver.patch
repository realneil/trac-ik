From: Peter Englert <penglert@rtr.ai>
Date: Fri, 27 Jan 2023 14:20:12 +0100
Subject: added wdls_ik_solver

---
 trac_ik_lib/include/trac_ik/kdl_tl.hpp | 10 +++++++
 trac_ik_lib/src/kdl_tl.cpp             | 48 ++++++++++++++++++++++++++--------
 2 files changed, 47 insertions(+), 11 deletions(-)

diff --git a/trac_ik_lib/include/trac_ik/kdl_tl.hpp b/trac_ik_lib/include/trac_ik/kdl_tl.hpp
index 45b60fc..12dd883 100644
--- a/trac_ik_lib/include/trac_ik/kdl_tl.hpp
+++ b/trac_ik_lib/include/trac_ik/kdl_tl.hpp
@@ -38,6 +38,7 @@ OF THE POSSIBILITY OF SUCH DAMAGE.
 #include <boost/math/tools/precision.hpp>
 #include <kdl/chainfksolverpos_recursive.hpp>
 #include <kdl/chainiksolvervel_pinv.hpp>
+#include <kdl/chainiksolvervel_wdls.hpp>
 
 namespace KDL {
 
@@ -49,6 +50,11 @@ enum BasicJointType { RotJoint, TransJoint, Continuous };
 /// via repeated application.
 class ChainIkSolverPos_TL {
  public:
+  ChainIkSolverPos_TL(const Chain& chain, const JntArray& q_min, const JntArray& q_max,
+                      bool use_wdls_solver, double eps_pos = 5e-4, double eps_rot = 5e-4,
+                      bool random_restart = false, bool try_jl_wrap = false, double task_weight = 1,
+                      double joint_weight = 1e-3, double lambda = 1e-3);
+
   ChainIkSolverPos_TL(const Chain& chain, const JntArray& q_min, const JntArray& q_max,
                       double eps = 1e-3, bool random_restart = false, bool try_jl_wrap = false);
 
@@ -104,11 +110,15 @@ class ChainIkSolverPos_TL {
   std::default_random_engine rng_;
 
   KDL::ChainIkSolverVel_pinv vik_solver_;
+  KDL::ChainIkSolverVel_wdls vik_solver_wdls_;
   KDL::ChainFkSolverPos_recursive fk_solver_;
 
   // step configuration
   KDL::Twist bounds_;
+  bool use_wdls_solver_;
   double eps_;
+  double eps_pos_;
+  double eps_rot_;
   bool rr_;
   bool wrap_;
 
diff --git a/trac_ik_lib/src/kdl_tl.cpp b/trac_ik_lib/src/kdl_tl.cpp
index 39ad93f..1f51528 100644
--- a/trac_ik_lib/src/kdl_tl.cpp
+++ b/trac_ik_lib/src/kdl_tl.cpp
@@ -44,14 +44,26 @@ namespace KDL {
 ChainIkSolverPos_TL::ChainIkSolverPos_TL(const Chain& chain, const JntArray& joint_min,
                                          const JntArray& joint_max, double eps, bool random_restart,
                                          bool try_jl_wrap)
+    : ChainIkSolverPos_TL(chain, joint_min, joint_max, false, eps * 0.5, eps * 0.5, random_restart,
+                          try_jl_wrap) {}
+
+ChainIkSolverPos_TL::ChainIkSolverPos_TL(const Chain& chain, const JntArray& joint_min,
+                                         const JntArray& joint_max, bool use_wdls_solver,
+                                         double eps_pos, double eps_rot, bool random_restart,
+                                         bool try_jl_wrap, double task_weight, double joint_weight,
+                                         double lambda)
     : chain_(chain),
       joint_min_(joint_min),
       joint_max_(joint_max),
+      use_wdls_solver_(use_wdls_solver),
       joint_types_(),
       vik_solver_(chain),
+      vik_solver_wdls_(chain),
       fk_solver_(chain),
       bounds_(KDL::Twist::Zero()),
-      eps_(eps),
+      eps_(eps_pos + eps_rot),
+      eps_pos_(eps_pos),
+      eps_rot_(eps_rot),
       rr_(random_restart),
       wrap_(try_jl_wrap),
       q_buff1_(chain_.getNrOfJoints()),
@@ -78,6 +90,15 @@ ChainIkSolverPos_TL::ChainIkSolverPos_TL(const Chain& chain, const JntArray& joi
   }
 
   assert(joint_types_.size() == joint_max.data.size());
+
+  if (use_wdls_solver_) {
+    Eigen::MatrixXd task_weights = Eigen::MatrixXd::Identity(6, 6) * task_weight;
+    Eigen::MatrixXd joint_weights =
+        Eigen::MatrixXd::Identity(chain_.getNrOfJoints(), chain_.getNrOfJoints()) * joint_weight;
+    vik_solver_wdls_.setWeightTS(task_weights);
+    vik_solver_wdls_.setWeightJS(joint_weights);
+    vik_solver_wdls_.setLambda(lambda);
+  }
 }
 
 int ChainIkSolverPos_TL::CartToJnt(const KDL::JntArray& q_init, const KDL::Frame& p_in,
@@ -88,7 +109,7 @@ int ChainIkSolverPos_TL::CartToJnt(const KDL::JntArray& q_init, const KDL::Frame
   const int max_iterations = 100;
   int res = 1;
   int i = 0;
-  while (i < max_iterations && res != 0) {
+  while (i < max_iterations && res != 0 && res != -100) {
     res = step();
 
     if (res == 2) {  // local minima => random restart
@@ -97,7 +118,7 @@ int ChainIkSolverPos_TL::CartToJnt(const KDL::JntArray& q_init, const KDL::Frame
     ++i;
   }
 
-  if (res == 0) {
+  if (res == 0 || res == -100) {
     q_out = qout();
   }
 
@@ -125,12 +146,16 @@ int ChainIkSolverPos_TL::step(int steps) {
   }
 
   for (int i = 0; i < steps; ++i) {
-    // ROS_ERROR_STREAM("KDL in: "<<f_curr_<<" "<<f_target_);
     KDL::Twist delta_twist = diff(f_curr_, f_target_);
 
-    // ROS_ERROR_STREAM("KDL in: "<<*q_curr_<<" "<<delta_twist.rot<<" "<<delta_twist.vel);
-    vik_solver_.CartToJnt(*q_curr_, delta_twist, delta_q_);
-    // ROS_ERROR_STREAM("KDL out: "<<delta_q_);
+    int error_code;
+    if (use_wdls_solver_ && chain_.getNrOfJoints() > 5) {
+      error_code = vik_solver_wdls_.CartToJnt(*q_curr_, delta_twist, delta_q_);
+    } else {
+      // The vik_solver_wdls_ solver is not able to handle robots with less than 6 joints
+      // (see https://github.com/orocos/orocos_kinematics_dynamics/issues/403)
+      error_code = vik_solver_.CartToJnt(*q_curr_, delta_twist, delta_q_);
+    }
 
     // apply delta to get the next configuration
     Add(*q_curr_, delta_q_, *q_next_);
@@ -236,11 +261,12 @@ int ChainIkSolverPos_TL::step(int steps) {
       delta_twist.rot.z(0);
     }
 
-    if (Equal(delta_twist, Twist::Zero(), eps_)) {
+    if (delta_twist.vel.Norm() <= eps_pos_ && delta_twist.rot.Norm() <= eps_rot_) {
       done_ = true;
-      // ROS_ERROR_STREAM("KDL final: "<<q_next_);
-
-      return 0;
+      if (error_code == SolverI::E_NOERROR) {
+        return 0;
+      }
+      return -100;
     }
   }
 
